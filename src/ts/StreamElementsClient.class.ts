export enum SEInterval {
  Alltime = 'alltime',
  Monthly = 'monthly',
  Weekly = 'weekly',
  Session = 'session'
}

export enum SEEventType{
  Cheer = 'cheer',
  Sub = 'sub',
  Tip = 'tip'
}

const API_BASE = "https://api.streamelements.com/kappa/v2/sessions/"

export class StreamElementsClient{
  constructor(
    private channelId: string,
    private apikey: string
  ) {}
  getTop(type: SEEventType, interval: SEInterval){
    return fetch(
      `${API_BASE}/${this.channelId}/top?type=${type}&interval=${interval}`,
      {
        headers: {
          'Authorization': 'apikey '+this.apikey
        }
      }
    )
    .then(resp => resp.json())
  }
  getTopCheers(interval = SEInterval.Alltime) {
    return this.getTop(SEEventType.Cheer, interval)
  }
  getTopTips(interval = SEInterval.Alltime) {
    return this.getTop(SEEventType.Tip, interval)
  }
}