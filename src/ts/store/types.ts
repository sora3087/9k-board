export type UserStat = {
  name: string
  amount: number
}