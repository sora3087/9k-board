import { combineReducers } from 'redux'
import { streamElementsReducers } from './streamElements/reducers'

export const rootReducer = combineReducers({
  se: streamElementsReducers
})