import { combineReducers } from 'redux'
import { cheersReducer } from './cheers/reducer'
import { tipsReducer } from './tips/reducer'

export const streamElementsReducers = combineReducers({
  cheers: cheersReducer,
  tips: tipsReducer
})