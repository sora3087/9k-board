import {
  CheersState,
  UpdateCheersActions,
  UPDATE_CHEERS_ALLTIME,
  UPDATE_CHEERS_MONTH,
  UPDATE_CHEERS_SESSION,
  UPDATE_CHEERS_WEEK
} from './types'

const initialState: CheersState = {
  alltime: [],
  month: [],
  week: [],
  session: []
}

export function cheersReducer(state = initialState, action: UpdateCheersActions){
  switch(action.type){
    case UPDATE_CHEERS_ALLTIME: {
      return {...state, alltime: action.data }
    }
    case UPDATE_CHEERS_MONTH: {
      return {...state, month: action.data }
    }
    case UPDATE_CHEERS_WEEK: {
      return {...state, week: action.data }
    }
    case UPDATE_CHEERS_SESSION: {
      return {...state, session: action.data }
    }
    default:
      return state
  }
}