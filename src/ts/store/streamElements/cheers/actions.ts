import { store } from '../..'
import { UserStat } from '../../types'
import { UPDATE_CHEERS_ALLTIME, UPDATE_CHEERS_MONTH, UPDATE_CHEERS_SESSION, UPDATE_CHEERS_WEEK } from './types'

export function UpdateCheersAlltime(data: Array<UserStat>){
  store.dispatch({
    type: UPDATE_CHEERS_ALLTIME,
    data
  })
}

export function UpdateCheersMonth(data: Array<UserStat>){
  store.dispatch({
    type: UPDATE_CHEERS_MONTH,
    data
  })
}

export function UpdateCheersWeek(data: Array<UserStat>){
  store.dispatch({
    type: UPDATE_CHEERS_WEEK,
    data
  })
}

export function UpdateCheersSession(data: Array<UserStat>){
  store.dispatch({
    type: UPDATE_CHEERS_SESSION,
    data
  })
}