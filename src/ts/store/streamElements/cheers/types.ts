import { UserStat } from '../../types'

export const UPDATE_CHEERS_ALLTIME = "UPDATE_CHEERS_ALLTIME"
export const UPDATE_CHEERS_MONTH = "UPDATE_CHEERS_MONTH"
export const UPDATE_CHEERS_WEEK = "UPDATE_CHEERS_WEEK"
export const UPDATE_CHEERS_SESSION = "UPDATE_CHEERS_SESSION"

type UpdateCheersAlltimeActionType = {
  type: typeof UPDATE_CHEERS_ALLTIME,
  data: Array<UserStat>
}

type UpdateCheersMonthActionType = {
  type: typeof UPDATE_CHEERS_MONTH,
  data: Array<UserStat>
}

type UpdateCheerWeekActionType = {
  type: typeof UPDATE_CHEERS_WEEK,
  data: Array<UserStat>
}

type UpdateCheersSessionActionType = {
  type: typeof UPDATE_CHEERS_SESSION,
  data: Array<UserStat>
}

export type CheersState = {
  alltime: Array<UserStat>
  month: Array<UserStat>
  week: Array<UserStat>
  session: Array<UserStat>
}

export type UpdateCheersActions =
  UpdateCheersAlltimeActionType
  | UpdateCheersMonthActionType
  | UpdateCheerWeekActionType
  | UpdateCheersSessionActionType