import {
  UpdateCheersAlltime,
  UpdateCheersMonth,
  UpdateCheersWeek,
  UpdateCheersSession
} from './store/streamElements/cheers/actions'
import { UserStat } from './store/types'
import { SEInterval, StreamElementsClient } from './StreamElementsClient.class'

const client = new StreamElementsClient("", "")

type SEEventData = {
  listener: string
}

window.addEventListener('onEventReceived', ((evt: CustomEvent<SEEventData>) => {
  switch(evt.detail.listener){
    case 'cheer-latest': {
      client.getTopCheers()
        .then(cheers => UpdateCheersAlltime(cheers))
      client.getTopCheers(SEInterval.Monthly)
        .then(cheers => UpdateCheersMonth(cheers))
      client.getTopCheers(SEInterval.Weekly)
        .then(cheers => UpdateCheersWeek(cheers))
      client.getTopCheers(SEInterval.Session)
        .then(cheers => UpdateCheersSession(cheers))
    }
    case 'tip-latest': {

    }
    default:
      return
  }
}) as EventListener)

type SELoadState = {
  "tip-session-top-donation": Array<UserStat>
  "tip-weekly-top-donation": Array<UserStat>
  "tip-monthly-top-donation": Array<UserStat>
  "tip-alltime-top-donation": Array<UserStat>
  "cheer-session-top-donation": Array<UserStat>
  "cheer-weekly-top-donation": Array<UserStat>
  "cheer-monthly-top-donation": Array<UserStat>
  "cheer-alltime-top-donation": Array<UserStat>
}

window.addEventListener('onWidgetLoad', ((seLoadEvent: CustomEvent<SELoadState>) => {
  const data = seLoadEvent.detail
  UpdateCheersAlltime(data['cheer-weekly-top-donation'])
  UpdateCheersMonth(data['cheer-monthly-top-donation'])
  UpdateCheersWeek(data['cheer-weekly-top-donation'])
  UpdateCheersSession(data['cheer-session-top-donation'])
}) as EventListener)